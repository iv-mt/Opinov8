data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

resource "aws_launch_configuration" "opinov8_test" {
  image_id       = data.aws_ami.ubuntu.id
  instance_type  = "t2.micro"
  security_group = [ aws_security_group.ssh.id, aws_security_group.http.id, aws_security_group.https.id ]

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "opinov8_test" {
  name                 = "terraform-asg-opinov8"
  launch_configuration = aws_launch_configuration.opinov8_test.name
  min_size             = var.asg_min
  max_size             = var.asg_max

  lifecycle {
    create_before_destroy = true
  }
}
