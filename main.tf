data "aws_availability_zones" "available" {
}

resource "aws_s3_bucket" "terraform_state" {
  bucket = "terraform-state-opinov8_test321123"

  versioning {
    enabled = true
  }

  lifecycle {
    prevent_destroy = true
  }

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
}

resource "aws_dynamodb_table" "terraform_locks" {
  name         = "terraform-state-opinov8_test321123"
  hash_key = "LockID"
  read_capacity = 20
  write_capacity = 20

  attribute {
    name = "LockID"
    type = "S"
  }
}

terraform {
  backend "s3" {
    bucket = "terraform-state-opinov8_test321123"
    key    = "global/tfstate/terraform.tfstate"
    region = "us-east-1"


    dynamodb_table = "terraform-state-lock"
    encrypt        = true
  }
}
