resource "aws_security_group" "ssh" {
  name        = "networking"
  description = "Allow SSH connection to instances."
  vpc_id      = aws_vpc.main.id

  ingress {
  from_port = 22
  to_port   = 22
  protocol  = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "http" {
  name        = "networking"
  description = "Allow HTTP connection to instances."
  vpc_id      = aws_vpc.main.id

  ingress {
  from_port = 80
  to_port   = 80
  protocol  = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "https" {
  name        = "networking"
  description = "Allow HTTPS connection to instances."
  vpc_id      = aws_vpc.main.id

  ingress {
  from_port = 443
  to_port   = 443
  protocol  = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
  }
}
