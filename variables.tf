variable "region" {
  description = "Region where resources will be deployed"
  type        = string

  default     = "us-east-1"
}

variable "state_bucket" {
  description = "S3 bucket name where tfstate will be stored."
  type        = string
  default     = "tfstate"
}

variable "asg_min" {
  description = "Min number of EC2 instances."
  type        = number
  default       = 3
}

variable "asg_max" {
  description = "Max number of EC2 instances."
  type        = number
  default       = 5
}
